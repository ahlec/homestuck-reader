<?php
$rss = simplexml_load_file("http://www.mspaintadventures.com/rss/rss.xml");
$finalPage = intval(substr($rss->channel[0]->item[0]->link, -6));
?>
<html>
	<head>
		<title>Homestuck Reader</title>
		<script type="text/javascript" src="jquery-2.0.3.min.js"></script>
		<script type="text/javascript">
			var LAST_PAGE = <?php echo $finalPage ?>;
		</script>
		<script type="text/javascript" src="scripts.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Marcellus+SC&subset=latin-ext,latin' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="style.css" />
		<script src="AC_RunActiveContent.js" language="javascript"></script>
	</head>
	<body>
		<div class="primaryPanel" id="primaryPanel">
			<div class="panelContainer" id="panelContainer">
				<div class="pageTitle" id="pageTitle"></div>
				<div class="primaryView" id="primaryView"></div>
				<div class="disableKeyboardContainer" id="disableKeyboardContainer">
					<input type="checkbox" id="disableKeyboard" /><label for="disableKeyboard">Disable Keyboard Shortcuts</label>
				</div>
				<div class="nextPageLink" id="nextPageLink"><span id="preLink"></span> <a id="link"></a></div>
			</div>
		</div>
		<div class="textPanel" id="textPanel"></div>
		<div class="loading" id="loading" style="display:none;"></div>
	</body>
</html>