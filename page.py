#!/usr/bin/python
import urllib
import re
import sys
import simplejson as json

# Get the raw HTML
urlRequest = urllib.urlopen("http://www.mspaintadventures.com/?s=6&p=" + sys.argv[1]).read().decode("utf-8")

# Get the comic segment only
isScratchRegex = re.compile(r"images/candycorn_scratch.png", re.S)
isScratchPage = (isScratchRegex.search(urlRequest) != None)

if (not isScratchPage):
    comicSegmentRegex = re.compile(r"<!-- begin comic content -->(.*)<!-- end comic content -->", re.S)
    comicSegmentMatch = comicSegmentRegex.search(urlRequest)
    if (comicSegmentMatch == None):
        raise Exception("There was a problem! Could not get comic content!")
    comicSegment = comicSegmentMatch.group(1)
else:
    scratchSegmentRegex = re.compile(r"FORUMS(.*)<table width=\"941\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#082a00\">(.*)<!--  FULL LOGO HEADER  -->", re.S)
    scratchSegmentMatch = scratchSegmentRegex.search(urlRequest)
    if (scratchSegmentMatch == None):
        raise Exception("There was a problem! Could not get scratch comic content!")
    comicSegment = scratchSegmentMatch.group(2)

# Split the raw HTML into the segments
segmentRegex = re.compile(r'images/v2_blank(strip|square2|square3)\.gif')
segments = segmentRegex.split(comicSegment)

#for segment in segments:
#    print(segment)
#    print("----------------------")
#
#print("====++=====++===++====++++")

# Get page title
titleRegex = re.compile(r'<font size="6">(.*)</font></p>', re.S)
titleMatch = titleRegex.search(segments[2])
if (titleMatch == None):
    raise Exception("Could not get the title!")
title = titleMatch.group(1).strip()

# Get image (if one exists)
imageRegex = re.compile(r'<img src="(.*?)">', re.S)
imageMatch = imageRegex.findall(segments[4])
hasImage = (imageMatch != None)
images = []
if (hasImage):
    for image in imageMatch:
        images.append(image.strip())
    hasImage = (len(images) > 0)

# Get page link (if one exists)
linkRegex = re.compile(r'<font size="5">(.*?)</font>', re.S)
if (isScratchPage):
    linkMatch = linkRegex.search(segments[-3])
else:
    linkMatch = linkRegex.search(segments[-1])
hasLink = (linkMatch != None)
link = None
preLink = None
if (hasLink):
    preLink = re.match(r'(.*)\s*<a', linkMatch.group(1).strip(), re.S).group(1).strip()
    link = re.search(r'<a(.*?)>(.*)</a>', linkMatch.group(1).strip(), re.S).group(2).strip()

# Get page text (if any exists)
textRegex = re.compile(r'<p(.*?)>(.*)</p>', re.S)
textMatch = textRegex.search(segments[6])
hasText = (textMatch != None)
text = None
if (hasText):
    text = textMatch.group(2).strip()
	
# Get flash (if one exists)
hasFlash = False
if not(hasImage):
    flashRegex = re.compile(r'<noscript>(.*)</noscript>', re.S)
    flashMatch = flashRegex.search(segments[4])
    hasFlash = (flashMatch != None)
    flash = None
    flashSetup = None
    if (hasFlash):
        flash = flashMatch.group(1).strip()
        flashSetupRegex = re.compile(r'AC_FL_RunContent\((.*?)\);\s\/\/end AC code', re.S)
        flashSetup = '<script language="javascript">AC_FL_RunContent(' + \
                     flashSetupRegex.search(segments[4]).group(1).strip() + ");</script>"

#print(title)
#print("hasImage: " + str(hasImage))
#print(image)
#print("hasLink: " + str(hasLink))
#print(preLink)
#print(link)
#print("hasText: " + str(hasText))
#print(text)

retJson = { }
retJson["title"] = title
retJson["isScratchLayout"] = isScratchPage
if (hasImage):
    retJson["images"] = images
if (hasLink):
    retJson["link"] = link
    retJson["preLink"] = preLink
if (hasText):
    retJson["text"] = text
if (hasFlash):
	retJson["flash"] = flash
	retJson["flashSetup"] = flashSetup

if (not(hasFlash) and not(hasImage) and not(hasLink) and (not(hasText) or text == "")):
	print("empty");
else:
	print(json.dumps(retJson))

