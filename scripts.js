function padPageNo(pageNo)
{
	var padded = pageNo.toString();
	while (padded.length < 6) {
		padded = '0' + padded;
	}
	return padded;
}

var FIRST_PAGE = 1901;
var PENDING_PAGE = null;

function getCurrentPage() {
	if (window.location.hash) {
		try {
			var pageNo = parseInt(window.location.hash.replace("#", ""));
			if (pageNo < FIRST_PAGE) {
				return FIRST_PAGE;
			}
			else if (pageNo > LAST_PAGE) {
				return LAST_PAGE;
			}
			return pageNo;
		}
		catch (e) {
			return FIRST_PAGE;
		}
	}
	else {
		return FIRST_PAGE;
	}
}

function getCurrentPercentage() {
	return Math.floor(((getCurrentPage() - FIRST_PAGE + 1) / (LAST_PAGE - FIRST_PAGE + 1)) * 100) + "%";
}

$(document).ready(function () {
	$("#loading").hide();
	loadPage(getCurrentPage());
		
	if ("onhashchange" in window) {
		$(window).bind('hashchange', function(e) {
			if (isPending()) {
				if (getCurrentPage() != PENDING_PAGE) {
					window.location.hash = "#" + PENDING_PAGE;
					console.log("resetting hash");
				}
			}
			else {
				loadPage(getCurrentPage());
			}
		});
	}
	else {
		$("#nextPageLink a#link").click(function () {
				loadPage($("#nextPageLink a#link").attr("href").substr(-6));
			});
	}
	
	$(window).resize(function () {
		// Explicitly set the height of the primary panel (which will allow it to scroll
		// if it overflows
		$("#panelContainer").css("height", $(window).height() + "px");
	});
	
	$(document).keydown(function (e) {
		if (e.which == 97 ||	// Numpad 1
			e.which == 46) {	// Delete
								// Did not bind "End" (Numpad 1 without Num Lock) because
								// the default action of this key might be used by readers.
			e.preventDefault();
			window.open('http://www.mspaintadventures.com/?s=6&p=' + padPageNo(getCurrentPage()));
			return;
		}
		else if (e.which == 96 ||	// Numpad 0
				 e.which == 45) {	// Insert/Numpad 0 (without Num Lock)
			e.preventDefault();
			alert('[PROGRESS]\nPage: ' + getCurrentPage() + '/' + LAST_PAGE + '\n' + getCurrentPercentage() + ' complete');
			return;
		}
	
		if (!isKeyboardEnabled()) {
			return;
		}
		
		var currentPage = getCurrentPage();
		
		if (e.which == 39) {	// right arrow
			e.preventDefault();
			if (currentPage < LAST_PAGE) {
				loadPage(currentPage + 1, "next");
			}
		}
		else if (e.which == 37) { 	// left arrow
			e.preventDefault();
			if (currentPage > FIRST_PAGE) {
				loadPage(currentPage - 1, "prev");
			}
		}
		else if (e.which == 80 || e.which == 220) {	// 'p' or '\'
			togglePesterchum();
		}
	});
});

function isPending() {
	return $("#loading").is(":visible");
}

function isKeyboardEnabled() {
	if (isPending()) {
		return false;
	}
	
	if (!$("#disableKeyboardContainer").is(":visible")) {
		return true;
	}
	
	return !$("#disableKeyboard").is(":checked");
}

function processPesterchumEntry(index, spn) {
	var span = $(spn);
	
	// Add background highlighting if we have a post from Doc Scratch
	if (span.css("color") == "rgb(255, 255, 255)") {
		span.addClass("whiteText");
	}
	
	// Style anything where we have exactly eight characters (VRISKA-ize)
	span.html(span.html().replace(/(\S)(?!\1)(\S)\2{7}(?!\2)/g, '$1<span class="vriska">$2$2$2$2$2$2$2$2</span>'));
};

function togglePesterchum() {
	var pesterchum = $("#textPanel > div");
	if (pesterchum.length == 0) {
		return;
	}
	
	var showDiv = pesterchum.children("div:not(.spoiler)"),
		hideDiv = pesterchum.children("div.spoiler"),
		isPesterchumShowing = hideDiv.is(":visible");
	if (isPesterchumShowing) {
		showDiv.show();
		hideDiv.hide();
	}
	else {
		showDiv.hide();
		hideDiv.show();
	}
}

function loadPage(pageNo, fromDir)
{
	if (isPending()) {
		if (getCurrentPage() != PENDING_PAGE) {
			window.location.hash = "#" + PENDING_PAGE;
		}
		return;
	}
	
	if (pageNo < FIRST_PAGE) {
		pageNo = FIRST_PAGE;
	}
	else if (pageNo > LAST_PAGE) {
		pageNo = LAST_PAGE;
	}
	
	$("#loading").show();
	$("body").addClass("isPending");
	PENDING_PAGE = pageNo;
	
	if (getCurrentPage() != pageNo) {
		window.location.hash = "#" + pageNo;
	}
	
	var primaryView = $("#primaryView"),
		nextPageLink = $("#nextPageLink"),
		preLink = $("#nextPageLink span#preLink"),
		link = $("#nextPageLink a#link")
	
	$.ajax({
		url: "page.php",
		type: "GET",
		data: {
			pageNo: padPageNo(pageNo)
		}
	}).done(function (data) {
		if (data == "empty") {
			$("#loading").hide();
			$("body").removeClass("isPending");
			loadPage((fromDir === "prev" ? pageNo - 1 : pageNo + 1), fromDir);
			return;
		}
		
		data = $.parseJSON(data);
		
		if (data.isScratchLayout) {
			$("html").addClass("scratch");
		}
		else {
			$("html").removeClass("scratch");
		}
		
		$("#pageTitle").html(data.title);
		
		// Update primary view
		if (data.images) {
			primaryView.empty();
			
			$.each(data.images, function (index, image) {
				var primaryViewImage = $(document.createElement("img"));
				primaryViewImage.attr("src", image);
				primaryView.append(primaryViewImage);
			});
		}
		else if (data.flash) {
			primaryView.empty();
			primaryView.html(data.flash);
		}
		
		if (data.flash) {
			$("#disableKeyboardContainer").show();
		}
		else {
			$("#disableKeyboardContainer").hide();
		}
		
		// Update next page link
		if (data.preLink) {
			preLink.html(data.preLink);
		}
		else {
			preLink.empty()
		}
		if (data.link) {
			link.attr("href", "#" + padPageNo(parseInt(pageNo) + 1));
			link.html(data.link);
			nextPageLink.show();
		}
		else {
			nextPageLink.hide();
		}
		
		$(window).resize();
		
		// Update text panel
		if (data.text) {
			$("body").addClass("hasTextPanel");
			$("#textPanel").html(data.text);
			$("#textPanel > div > div button").text(function (index, oldText) {
				return oldText + " [P]";
			});
			$("#textPanel > div > div.spoiler > table p > span").each(processPesterchumEntry);
			togglePesterchum();
		}
		else {
			$("body").removeClass("hasTextPanel");
		}
		
		$("#loading").hide();
		$("body").removeClass("isPending");
	}).fail(function (data) {
		alert("failed");
		console.dir(data);
		$("#loading").hide();
		$("body").removeClass("isPending");
	});
}