<?php
if (!isset($_GET["pageNo"]))
{
	exit("{success: false, message: 'no pageNo'}");
}
$pageNo = $_GET["pageNo"];
if (!ctype_digit($pageNo))
{
	exit("{success: false, message: 'pageNo not numeric'}");
}

exit(exec("python page.py " . $pageNo));
?>